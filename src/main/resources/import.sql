


INSERT INTO PROPERTY_OWNER (vat,firstname,lastname,email,address,phoneNumber,property_type,role,password) VALUES (1212123,'Leo', 'Tolstoy','tolstoy@test.com','kresnis 4','210544312','MAISONETTE','USER', '$2a$10$dwF44b7vyxH0qKY2Q5Bi1ulVUwjxv9L5UmEgBKfcCU9WtpqXwjsy.');
INSERT INTO PROPERTY_OWNER (vat,firstname,lastname,email,address,phoneNumber,property_type,role,password) VALUES (1212152,'John', 'Steinbeck','stein@test.com','palalama 10','2104546481','DETACHED','USER', '$2a$10$dwF44b7vyxH0qKY2Q5Bi1ulVUwjxv9L5UmEgBKfcCU9WtpqXwjsy.');
INSERT INTO PROPERTY_OWNER (vat,firstname,lastname,email,address,phoneNumber,property_type,role,password) VALUES (1243123,'Alexandros', 'Papadiamantis','pap@test.com','papanikoli 2','2103454191','APARTMENT','USER', '$2a$10$dwF44b7vyxH0qKY2Q5Bi1ulVUwjxv9L5UmEgBKfcCU9WtpqXwjsy.');
INSERT INTO PROPERTY_OWNER (vat,firstname,lastname,email,address,phoneNumber,property_type,role,password) VALUES (1354123,'Nikos', 'Kazantzakis','kazatz@test.com','karea 50','2102365678','DEFAULT','ADMIN', '$2a$10$dwF44b7vyxH0qKY2Q5Bi1ulVUwjxv9L5UmEgBKfcCU9WtpqXwjsy.');
INSERT INTO PROPERTY_OWNER (vat,firstname,lastname,email,address,phoneNumber,property_type,role,password) VALUES (1247841,'Angelos', 'Terzakis','angterz@test.com','giannou 6','2107687321','MAISONETTE','USER', '$2a$10$dwF44b7vyxH0qKY2Q5Bi1ulVUwjxv9L5UmEgBKfcCU9WtpqXwjsy.');


INSERT INTO REPAIR (repair_id,vat,description,repair_address,repair_cost,repair_status,repair_type,repair_date) VALUES (1,1212123,'started floor','tinou11',10000.99,'PENDING','PAINTING','2018-6-5');
INSERT INTO REPAIR (repair_id,vat,description,repair_address,repair_cost,repair_status,repair_type,repair_date) VALUES (2,1212123,'fixing door','tinou11',1500,'PENDING','INSULATION','2018-6-5');
INSERT INTO REPAIR (repair_id,vat,description,repair_address,repair_cost,repair_status,repair_type,repair_date) VALUES (3,1212152,'started ceiling','romilias55',3000.35,'PROGRESS','FRAMING','2019-8-7');
INSERT INTO REPAIR (repair_id,vat,description,repair_address,repair_cost,repair_status,repair_type,repair_date) VALUES (4,1212123,'started ceiling','tinou11',4500,'COMPLETED','FRAMING','2019-1-2');
INSERT INTO REPAIR (repair_id,vat,description,repair_address,repair_cost,repair_status,repair_type,repair_date) VALUES (5,1212123,'create saloon','tinou11',2500.9,'PENDING','FRAMING','2019-4-5');