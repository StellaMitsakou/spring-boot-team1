jQuery(function ($){

    const $createRepairForm = $('#createRepairForm');
    if ($createRepairForm.validate) {
        $createRepairForm.validate({
            rules: {
                repairId: {
                    required: true
                },
                repairDate: {
                    required: true
                },
                repairCost:{
                    required:true,
                },
                repairAddress: {
                    required:true
                },
                ownerId: {
                    required:true,
                    digits: true,
                    minlength: 7 ,
                    maxlength: 7
                }
            },
            messages: {
                repairId: {
                    required: "Repair Id is required."
                },
                repairDate: {
                    required: "Date is required."
                },
                repairCost: {
                    required: "Cost is required."
                },
                repairAddress: {
                  required: "Address is required."
                },
                ownerId: {
                    minlength: "It should be at 7 digits.",
                    maxlength: "It should be at 7 digits.",
                    required: "Owner Id is required."
                }
            }
        });
    }

    const $searchByDate = $('#searchByDate');
    if ($searchByDate.validate) {
        $searchByDate.validate({
            rules: {
                repairDate: {
                    required: true
                }
            },
            messages: {
                repairDate: {
                    required: "Date is required."
                }
            }
        })
        }

    const $searchByPeriod = $('#searchByPeriod');
    if ($searchByPeriod.validate) {
        $searchByPeriod.validate({
            rules: {
                repairDate: {
                    required: true
                },
                repairDate2: {
                    required: true
                }
            },
            messages: {
                repairDate: {
                    required: "Date is required."
                },
                repairDate2: {
                    required: "Date is required."
                }
            }
        })
    }

    const $searchByVat = $('#searchByVat');
    if ($searchByVat.validate) {
        $searchByVat.validate({
            rules: {
                ownerId: {
                    required: true,
                    digits: true,
                    minlength:7,
                    maxlength:7,

                }
            },
            messages: {
                ownerId: {
                    required: "Vat is required.",
                    minlength:"It should be at 7 digits" ,
                    maxlength: "It should be at 7 digits"
                }
            }
        })
    }

    const $editRepair = $('#editRepair');
    if ($editRepair.validate) {
        $editRepair.validate({
            rules: {
                date: {
                    required: true
                },
                statusType:{
                    required:true,
                },
                repairType: {
                    required:true
                },
                cost:{
                    required:true
                },
                address: {
                    required:true
                }
            },
            messages: {
                repairId: {
                    required: "Repair Id is required.",
                },
                repairDate: {
                    required: "Date is required."
                },
                repairCost: {
                    required: "Cost is required."
                },
                repairAddress: {
                    required: "Address is required."
                },
                ownerId: {
                    minlength: "It should be at 7 digits.",
                    maxlength: "It should be at 7 digits.",
                    required: "Owner Id is required."
                }
            }
        });
    }
});