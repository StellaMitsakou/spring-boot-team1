package com.codehub.academy.springbootteam1.repository;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PropertyOwnerRepository extends JpaRepository<PropertyOwner,Long> {

    Optional<PropertyOwner> findByEmail(String email);

}
