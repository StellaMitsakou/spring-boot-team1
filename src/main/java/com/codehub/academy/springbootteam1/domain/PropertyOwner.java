package com.codehub.academy.springbootteam1.domain;


import com.codehub.academy.springbootteam1.enums.PropertyTypeEnum;
import com.codehub.academy.springbootteam1.enums.RoleTypeEnum;

import javax.persistence.*;

@Entity
@Table(name = "PROPERTY_OWNER", uniqueConstraints = {@UniqueConstraint(columnNames = {"firstname", "lastname"})})
public class PropertyOwner {

    private static final int MAX_NAME_LENGTH = 60;

    @Id
    @Column(name = "vat",nullable = false)
    private Long vat;

    @Column(name = "firstname", length = MAX_NAME_LENGTH)
    private String firstName;

    @Column(name = "lastName", length = MAX_NAME_LENGTH)
    private String lastName;

    @Column(name = "address", length = MAX_NAME_LENGTH)
    private String address;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "email", length = MAX_NAME_LENGTH)
    private String email;

    @Column(name = "password", length = MAX_NAME_LENGTH)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "property_type")
    private PropertyTypeEnum propertyType;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private RoleTypeEnum role;

    public PropertyOwner(Long vat,
                         String firstName,
                         String lastName,
                         String address,
                         String phoneNumber,
                         String email,
                         String password,
                         PropertyTypeEnum propertyType,
                         RoleTypeEnum role) {
        this.vat = vat;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.propertyType = propertyType;
        this.role = role;
    }

    public PropertyOwner() {
    }

    public static int getMaxNameLength() {
        return MAX_NAME_LENGTH;
    }

    public long getVat() {
        return vat;
    }

    public void setVat(long vat) {
        this.vat = vat;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PropertyTypeEnum getPropertyType() {
        return propertyType;
    }

    public void setPropertyTypeEnum(PropertyTypeEnum propertyType) {
        this.propertyType = propertyType;
    }

    public RoleTypeEnum getRole() { return role;}

    public void setRole(RoleTypeEnum role) {
        this.role = role;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PropertyOwner{");
        sb.append("vat=").append(vat);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", phoneNumber=").append(phoneNumber);
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", propertyType=").append(propertyType).append('\'');
        sb.append(", role=").append(role).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
