package com.codehub.academy.springbootteam1.controller.owners;

import com.codehub.academy.springbootteam1.forms.SearchOwnerForm;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import com.codehub.academy.springbootteam1.service.PropertyOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin/owners")
public class SearchOwnerController {

    private static final String OWNERS_LIST = "owners";

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @GetMapping(value = "")
    public String showOwners(Model model){
        List<PropertyOwnerModel> owners = propertyOwnerService.findAll();
        model.addAttribute(OWNERS_LIST, owners);
        return "pages/searchOwner";
    }

    @PostMapping("/vat")
    public String showOwnerByVat(@ModelAttribute("searchOwnerForm") SearchOwnerForm searchOwnerForm,
                             Model model) {
        PropertyOwnerModel propertyOwnerModel = propertyOwnerService.findByVat(Long.valueOf(searchOwnerForm.getVat()));

        model.addAttribute(OWNERS_LIST, List.of(propertyOwnerModel));
        return "pages/searchOwner";
    }

    @PostMapping("/email")
    public String showOwnerByEmail(@ModelAttribute("searchOwnerForm") SearchOwnerForm searchOwnerForm,
                             Model model) {
        PropertyOwnerModel propertyOwnerModel = propertyOwnerService.findByEmail(searchOwnerForm.getEmail());

        model.addAttribute(OWNERS_LIST, List.of(propertyOwnerModel));
        return "pages/searchOwner";
    }

}


