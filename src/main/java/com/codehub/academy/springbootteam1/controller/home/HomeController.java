package com.codehub.academy.springbootteam1.controller.home;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.enums.RoleTypeEnum;
import com.codehub.academy.springbootteam1.model.LoginResponse;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    private static final String REPAIRS_LIST = "repairs";

    @Autowired
    private RepairService repairService;

    @GetMapping(value = "/user/home")
    public String userRepairs(Model model) {
        SecurityContext contextHolder = SecurityContextHolder.getContext();
        LoginResponse loginResponse = (LoginResponse) contextHolder.getAuthentication().getPrincipal();

        PropertyOwner propertyOwner = loginResponse.getPropertyOwner();
        List<RepairModel> ownerRepairs = repairService.findByPropertyOwnerId(propertyOwner.getVat());

        model.addAttribute("repairs", ownerRepairs);
        model.addAttribute("userFirstName", propertyOwner.getFirstName());
        model.addAttribute("role", propertyOwner.getRole().name());
        return "pages/repairs_of_day";
    }

    @GetMapping(value = "/admin/home")
    public String repairsOfDay(Model model){
        List<RepairModel> repairs = repairService.findAll();
        model.addAttribute(REPAIRS_LIST, repairs);
        model.addAttribute("role", RoleTypeEnum.ADMIN.name());
        return "pages/repairs_of_day";
    }

}
