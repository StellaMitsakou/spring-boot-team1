package com.codehub.academy.springbootteam1.controller.owners;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.enums.PropertyTypeEnum;
import com.codehub.academy.springbootteam1.enums.RepairStatusEnum;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.service.PropertyOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/owners")
public class OwnerController {

    private static final String OWNERS_ATTR = "owner";
    private static final String OWNERS_PROPTYPE = "ownerPropertyTypes";

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @GetMapping({"/create"})
    public String first(Model model, @RequestParam(value = "name", required = false, defaultValue = "World") String name) {
        model.addAttribute("name", name);
        return "pages/createOwner";
    }

    @GetMapping({"/{id}/edit"})
    public String getEditOwner(@PathVariable Long id,Model model) {
        PropertyOwnerModel propertyOwnerModel = propertyOwnerService.findByVat(id);
        model.addAttribute(OWNERS_ATTR, propertyOwnerModel);
        model.addAttribute(OWNERS_PROPTYPE, PropertyTypeEnum.values());
        return "/pages/editOwner";
    }

    @PostMapping({"/{id}"})
    public String doEditOwner(@PathVariable Long id, PropertyOwnerModel propertyOwnerModel) {
        //System.out.println(propertyOwnerModel.getVat());
        propertyOwnerService.updateOwner(propertyOwnerModel);
        return "redirect:/admin/owners";
    }

    @PostMapping({"/{id}/delete"})
    public String deleteOwner(@PathVariable Long id) {
        propertyOwnerService.deleteByVat(id);
        return "redirect:/admin/owners";
    }

}
