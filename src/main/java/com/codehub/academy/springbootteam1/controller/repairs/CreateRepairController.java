package com.codehub.academy.springbootteam1.controller.repairs;

import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.enums.RepairStatusEnum;
import com.codehub.academy.springbootteam1.enums.RepairTypeEnum;
import com.codehub.academy.springbootteam1.forms.SearchRepairForm;
import com.codehub.academy.springbootteam1.mapper.RepairFormToRepair;
import com.codehub.academy.springbootteam1.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/repairs")
public class CreateRepairController {

    private static final String REPAIR_ATTR = "repair";
    private static final String REPAIR_STATUS ="repairStatuses";
    private static final String REPAIR_TYPE ="repairType";

    @Autowired
    private RepairService repairService;

    @Autowired
    private RepairFormToRepair mapper;

    @GetMapping({"/create"})
    public String createRepair(Model model) {
        model.addAttribute(REPAIR_ATTR, new SearchRepairForm());
        model.addAttribute(REPAIR_STATUS, RepairStatusEnum.values());
        model.addAttribute(REPAIR_TYPE, RepairTypeEnum.values());
        return "/pages/createRepair";
    }

    @PostMapping({"/create"})
    public String createRepair(Model model,
                               @Valid @ModelAttribute(REPAIR_ATTR) SearchRepairForm repairForm,
                               BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            System.out.println("This was errored out!");
            return "/pages/createRepair";
        }

        Repair repair = mapper.toRepair(repairForm);
        repairService.createRepair(repair);

        return "redirect:/admin/repairs";
    }

}
