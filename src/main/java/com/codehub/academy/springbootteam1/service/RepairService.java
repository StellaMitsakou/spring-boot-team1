package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.model.RepairModel;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RepairService {

    //    SEARCH by Date (Equals, After, Between) and by Owner Id functionality

    List<RepairModel> findByRepairDateAfter(LocalDate repairDate);

    List<RepairModel> findByRepairDateEquals(LocalDate repairDate);

    List<RepairModel> findByRepairDateBetween(LocalDate startDate, LocalDate endDate);

    List<RepairModel> findByPropertyOwnerId(Long id);

    List<RepairModel> findAll();

    List<Repair> findByOwnerId(Long id);

    // DELETE by Id

    void deleteById(Long id);

    RepairModel findById(Long id);

    //CREATE / REPAIR

    public Repair updateRepair(RepairModel repairModel);

    public void createRepair(Repair repair);

}