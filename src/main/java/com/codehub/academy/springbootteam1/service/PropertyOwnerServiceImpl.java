package com.codehub.academy.springbootteam1.service;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.mapper.PropertyOwnerToPropertyOwnerModel;
import com.codehub.academy.springbootteam1.model.PropertyOwnerModel;
import com.codehub.academy.springbootteam1.model.RepairModel;
import com.codehub.academy.springbootteam1.repository.PropertyOwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class PropertyOwnerServiceImpl implements PropertyOwnerService {

    @Autowired
    private PropertyOwnerRepository propertyOwnerRepository;

    @Autowired
    private RepairService repairService;


    @Autowired
    private PropertyOwnerToPropertyOwnerModel mapper;

//    SEARCH by Vat and Email functionality

    @Override
    public PropertyOwnerModel findByVat(Long vat) {
        PropertyOwner propertyOwner = propertyOwnerRepository.findById(vat).get();
        return mapper.mapToPropertyOwnerModel(propertyOwner);
//        PropertyOwner propertyOwner = findById(vat).get();
//        return mapper.mapToPropertyOwnerModel(propertyOwner);
    }

    //is it used ??
    @Override
    public Optional<PropertyOwner> findById(Long id) {
        return propertyOwnerRepository.findById(id);
    }

    @Override
    public PropertyOwnerModel findByEmail(String email) {
        PropertyOwner propertyOwner = propertyOwnerRepository.findByEmail(email).get();
        return mapper.mapToPropertyOwnerModel(propertyOwner);
    }

    @Override
    public List<PropertyOwnerModel> findAll() {
        return propertyOwnerRepository
                .findAll()
                .stream()
                .map(owner -> mapper.mapToPropertyOwnerModel(owner))
                .collect(Collectors.toList());
    }


    // DELETE

    @Override
    public void deleteByVat(Long id) {

        //first delete owner repairs
        List<Repair> repairs = repairService.findByOwnerId(id);

        for(Iterator<Repair> i = repairs.iterator(); i.hasNext();){
            Repair repair = i.next();
            repairService.deleteById(repair.getRepairId());
            i.remove();
        }

        //then delete their owner
        propertyOwnerRepository.deleteById(id);
    }

    // UPDATE

    @Override
    public PropertyOwner updateOwner(PropertyOwnerModel ownerModel) {

//        System.out.println(ownerModel.getVat());
        PropertyOwner propertyOwner = propertyOwnerRepository.findById(Long.valueOf(ownerModel.getVat())).get();

        propertyOwner.setAddress(ownerModel.getAddress());
        propertyOwner.setEmail(ownerModel.getEmail());
        propertyOwner.setFirstName(ownerModel.getFirstName());
        propertyOwner.setLastName(ownerModel.getLastName());
        propertyOwner.setPassword(ownerModel.getPassword());
        propertyOwner.setPhoneNumber(ownerModel.getPhoneNumber());
        propertyOwner.setPropertyTypeEnum(ownerModel.getPropertyType());


        propertyOwner.setVat(Long.valueOf(ownerModel.getVat())); //not to be changed in edit
        propertyOwner.setRole(ownerModel.getRole());

        return propertyOwnerRepository.save(propertyOwner);
    }


//    @Override
//    public Optional<PropertyOwner> createPropertyOwner(PropertyOwner propertyOwner) {
//        return propertyOwnerRepository.save(propertyOwner);
//    }
}
