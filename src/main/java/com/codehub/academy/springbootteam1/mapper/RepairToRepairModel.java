package com.codehub.academy.springbootteam1.mapper;

import com.codehub.academy.springbootteam1.domain.Repair;
import com.codehub.academy.springbootteam1.enums.RepairStatusEnum;
import com.codehub.academy.springbootteam1.enums.RepairTypeEnum;
import com.codehub.academy.springbootteam1.model.RepairModel;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

@Component
public class RepairToRepairModel {

    public RepairModel mapToRepairModel(Repair repair){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        RepairModel repairModel = new RepairModel();

        repairModel.setId(repair.getRepairId().toString());
        repairModel.setDate(dateTimeFormatter.format(repair.getRepairDate()));

        repairModel.setStatusType(repair.getRepairStatus() != null ? repair.getRepairStatus() : RepairStatusEnum.DEFAULT);
        repairModel.setRepairType(repair.getRepairType() != null ? repair.getRepairType() : RepairTypeEnum.DEFAULT);
        repairModel.setCost(String.valueOf(repair.getRepairCost()));
        repairModel.setAddress(repair.getRepairAddress());
        repairModel.setDescription(repair.getDescription());
        repairModel.setOwnerId(String.valueOf(repair.getPropertyOwner().getVat()));

        return repairModel;
    }

}

