package com.codehub.academy.springbootteam1.model;

import com.codehub.academy.springbootteam1.enums.RepairStatusEnum;
import com.codehub.academy.springbootteam1.enums.RepairTypeEnum;

import java.util.StringJoiner;

public class RepairModel {

    private String id;
    private String date;
    private RepairStatusEnum statusType;
    private RepairTypeEnum repairType;
    private String cost;
    private String address;
    private String description;
    private String ownerId;

    public RepairModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RepairStatusEnum getStatusType() {
        return statusType;
    }

    public void setStatusType(RepairStatusEnum statusType) {
        this.statusType = statusType;
    }

    public RepairTypeEnum getRepairType() {
        return repairType;
    }

    public void setRepairType(RepairTypeEnum repairType) {
        this.repairType = repairType;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    //no repairType :(
    @Override
    public String toString() {
        return new StringJoiner(", ", RepairModel.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("date='" + date + "'")
                .add("status='" + statusType + "'")
                .add("type='" + repairType + "'")
                .add("cost='" + cost + "'")
                .add("address='" + address + "'")
                .add("description='" + description + "'")
//                .add("ownerId='" + ownerId + "'")
                .toString();
    }
}
