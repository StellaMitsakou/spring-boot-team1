package com.codehub.academy.springbootteam1.model;

import com.codehub.academy.springbootteam1.domain.PropertyOwner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class LoginResponse extends User {

    private PropertyOwner propertyOwner;

    public LoginResponse(String username, String password, Collection<? extends GrantedAuthority> authorities, PropertyOwner propertyOwner) {
        super(username, password, authorities);
        this.propertyOwner = propertyOwner;
    }

    public PropertyOwner getPropertyOwner() {
        return propertyOwner;
    }
}
